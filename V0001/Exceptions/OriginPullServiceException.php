<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions;

/**
 * Class OriginPullServiceException
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions
 */
class OriginPullServiceException extends \Exception
{

} 