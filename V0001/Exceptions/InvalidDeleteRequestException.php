<?php
/**
 * User: Fabrice Baumann - capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 04/03/16 - 9:22 AM
 */

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions;


/**
 * Class InvalidDeleteRequestException
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions
 */
class InvalidDeleteRequestException extends \Exception
{

}