<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions;

/**
 * Class JobIdMissingInFileException
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions
 */
class JobIdMissingInFileException extends \Exception
{

} 