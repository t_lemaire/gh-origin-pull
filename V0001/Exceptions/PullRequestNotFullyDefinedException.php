<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions;

/**
 * Class PullRequestNotFullyDefinedException
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions
 */
class PullRequestNotFullyDefinedException extends \Exception
{

} 