<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;


abstract class FilesContainerRequest extends Request {
    /**
     * @var array File[]
     */
    private $files = array();

    /**
     * @param array $files
     *
     * @return self
     */
    public function setFiles($files)
    {
        foreach ($files as $key => $value) {
            $this->addFile($value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param File $file
     *
     * @return self
     */
    public function addFile(File $file)
    {
        $this->files[md5($file->getUrl())] = $file;

        return $this;
    }


} 