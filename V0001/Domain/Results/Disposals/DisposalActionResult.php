<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\Disposals;

use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\DisposalAction;

class DisposalActionResult
{
    const STATUS_FAILED = 'failed';
    const STATUS_MOVED = 'moved';
    const STATUS_DELETED = 'deleted';

    /**
     * @var DisposalAction The action to which the result relates.
     */
    protected $action;

    /**
     * @var string
     */
    protected $status = '';

    /**
     * @var string
     */
    protected $error = '';

    /**
     * @return DisposalAction
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param DisposalAction $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }



    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function wasSuccessful()
    {
        return ($this->status === self::STATUS_MOVED || $this->status === self::STATUS_DELETED);
    }

    /**
     * @return bool
     */
    public function wasUnsuccessful()
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }


}