<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\Disposals;


use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class DisposalResult extends Result
{

    /**
     * @var DisposalActionResult[] The results, in the same order as the requested actions.
     */
    protected $actionResults;

    /**
     * @return DisposalActionResult[]
     */
    public function getActionResults()
    {
        return $this->actionResults;
    }

    /**
     * @param DisposalActionResult[] $actionResults
     * @return $this
     */
    public function setActionResults($actionResults)
    {
        $this->actionResults = $actionResults;
        return $this;
    }

    /**
     * @param string|int $key
     * @param DisposalActionResult $actionResult
     * @return $this
     */
    public function setActionResult($key, $actionResult)
    {
        $this->actionResults[$key] = $actionResult;
        return $this;
    }


    /**
     * @return bool True iff all actions were successful.
     */
    public function wasSuccessful()
    {
        foreach ($this->actionResults as $actionResult) {
            if ($actionResult->wasUnsuccessful()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return bool True iff at least one action was unsuccessful.
     */
    public function wasUnsuccessful()
    {
        return !$this->wasSuccessful();
    }

    /**
     * @return string The first error that happened, or '' if none.
     */
    public function getError()
    {
        foreach ($this->actionResults as $actionResult) {
            if ($actionResult->wasUnsuccessful()) {
                return $actionResult->getError();
            }
        }
        return '';
    }

    /**
     * @return bool True if any action succeeded.
     */
    public function didAnyActionsSucceed()
    {
        foreach ($this->actionResults as $actionResult) {
            if ($actionResult->wasSuccessful()) {
                return true;
            }
        }
        return false;
    }


}