<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\NodeActions;


use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class NodeActionsResult extends Result
{

    /**
     * @var NodeResult[]
     */
    private $nodes = array();

    /**
     * @var string
     */
    private $referenceId = '';

    /**
     * @param NodeResult[] $nodes
     *
     * @return self
     */
    public function setNodeResults($nodes)
    {
        foreach ($nodes as $key => $value) {
            $this->addNodeResult($value);
        }

        return $this;
    }

    /**
     * @return NodeResult[]
     */
    public function getNodeResults()
    {
        return $this->nodes;
    }

    /**
     * @param NodeResult $node
     *
     * @return self
     */
    public function addNodeResult(NodeResult $node)
    {
        $this->nodes[md5($node->getPath())] = $node;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param string $referenceId
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
    }

}
