<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\NodeActions;


use GorillaHub\FilesBundle\Domain\Node;

class NodeResult extends Node
{
    const STATUS_FAILED = 'failed';
    const STATUS_MOVED = 'moved';
    const STATUS_COPIED = 'copied';


    /**
     * @var string
     */
    protected $status = '';

    /**
     * @var string
     */
    protected $error = '';

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function wasSuccessful()
    {
        return ($this->status === self::STATUS_MOVED || $this->status === self::STATUS_COPIED);
    }

    /**
     * @return bool
     */
    public function wasUnsuccessful()
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param mixed $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }


}