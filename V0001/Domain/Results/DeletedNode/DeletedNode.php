<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\DeletedNode;


use \GorillaHub\FilesBundle\Domain\Node;

class DeletedNode extends Node
{
    const STATUS_FAILED = 'failed';
    const STATUS_DELETED = 'deleted';
    const STATUS_NOT_FOUND = 'not_found';


    /**
     * @var string
     */
    protected $status = '';


    /**
     * @return bool
     */
    public function wasSuccessful()
    {
        return ($this->status === self::STATUS_DELETED || $this->status === self::STATUS_NOT_FOUND);
    }

    /**
     * @return bool
     */
    public function wasUnsuccessful()
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}