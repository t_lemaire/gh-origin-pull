<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results;


use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeFailure;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class InvalidDeleteRequestResult  extends Result {
	/**
	 * @var string
	 */
	private $message = '';

	/* @var NodeFailure[] */
	private $nodeFailures = array();

	/**
	 * @param string $message
	 *
	 * @return $this
	 */
	final public function setMessage($message)
	{
		$this->message = $message;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeFailure[] $nodeFailures
	 * @return $this
	 */
	public function setNodeFailures($nodeFailures)
	{
		$this->nodeFailures = $nodeFailures;
		return $this;
	}

	/**
	 * @return \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeFailure[]
	 */
	public function getNodeFailures()
	{
		return $this->nodeFailures;
	}

	/**
	 * @param \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeFailure $nodeFailure
	 * @return $this
	 */
	public function addNodeFailure($nodeFailure){
		if($this->nodeFailures==null){
			$this->nodeFailures = array();
		}

		$this->nodeFailures[] = $nodeFailure;

		return $this;
	}


} 