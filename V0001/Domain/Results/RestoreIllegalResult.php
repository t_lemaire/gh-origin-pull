<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

/**
 * This result is the response to a request to /originpull/restoreillegal/, if that request is successful.  See
 * RestoreIllegalRequest.
 */
class RestoreIllegalResult extends Result
{

    /**
     * @var string The URL at which the file can be (temporarily) downloaded.
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }


}

