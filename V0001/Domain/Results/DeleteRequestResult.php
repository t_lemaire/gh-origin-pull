<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results;

use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\DeletedNode\DeletedNode;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Result;

class DeleteRequestResult extends Result
{

	/**
	 * @var array DeleteNode[]
	 */
	private $deletedNodes = array();

	/**
	 * @param array $nodes
	 *
	 * @return self
	 */
	public function setDeletedNodes($nodes)
	{
		foreach ($nodes as $key => $value) {
			$this->addDeletedNode($value);
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function getDeletedNodes()
	{
		return $this->deletedNodes;
	}

	/**
	 * @param DeletedNode $deletedNode
	 *
	 * @return self
	 */
	public function addDeletedNode(DeletedNode $deletedNode)
	{
		$this->deletedNodes[md5($deletedNode->getPath())] = $deletedNode;

		return $this;
	}

}

