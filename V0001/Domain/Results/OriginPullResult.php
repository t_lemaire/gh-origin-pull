<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;

class OriginPullResult extends FilesContainerResult
{
	/**
	 * @var string
	 */
	private $referenceId;

	/**
	 * @var FilePattern
	 */
	private $filePattern;

	/**
	 * @var string
	 */
	private $basePath = '';

	/**
	 * @param string $referenceId
	 */
	public function setReferenceId($referenceId)
	{
		$this->referenceId = $referenceId;
	}

	/**
	 * @return string
	 */
	public function getReferenceId()
	{
		return $this->referenceId;
	}

	/**
	 * @param \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern $filePattern
	 */
	public function setFilePattern($filePattern)
	{
		$this->filePattern = $filePattern;
	}

	/**
	 * @return \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern
	 */
	public function getFilePattern()
	{
		return $this->filePattern;
	}

	/**
	 * @param string $basePath
	 */
	public function setBasePath($basePath)
	{
		$this->basePath = $basePath;
	}

	/**
	 * @return string
	 */
	public function getBasePath()
	{
		return $this->basePath;
	}

}