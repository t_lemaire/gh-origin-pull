<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use \GorillaHub\FilesBundle\Domain\Node;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;

/**
 * Class DeleteRequest
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Domain
 */
class DeleteRequest extends Request implements SDKCallInterface
{

	/**
	 * @var array Node[]
	 */
	private $nodes = array();

	/**
	 * @var array Node[]
	 */
	private $excludedNodes = array();

	/**
	 * @var boolean
	 */
	private $recursiveDelete = false;

	/**
	 * @var int
	 */
	private $minimumPathDepth = 5;

	/**
	 * @return int
	 */
	public function getMinimumPathDepth()
	{
		return $this->minimumPathDepth;
	}

	/**
	 * @param int $minimumPathDepth The minimum path depth of a node to delete.  For example, a setting of 2 would
	 * 		allow /path1/path2 to be deleted, but not /path1 or /
	 * @return DeleteRequest
	 */
	public function setMinimumPathDepth($minimumPathDepth)
	{
		$this->minimumPathDepth = $minimumPathDepth;
		return $this;
	}

	/**
	 * @param array $nodes
	 *
	 * @return self
	 */
	public function setNodes($nodes)
	{
		foreach ($nodes as $key => $value) {
			$this->addNode($value);
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function getNodes()
	{
		return $this->nodes;
	}

	/**
	 * @param \GorillaHub\FilesBundle\Domain\Node $node
	 *
	 * @return self
	 */
	public function addNode(Node $node)
	{
		$this->nodes[md5($node->getPath())] = $node;

		return $this;
	}


	/**
	 * @param array $excludedNodes
	 *
	 * @return self
	 */
	public function setExcludedNodes($excludedNodes)
	{
		foreach ($excludedNodes as $key => $value) {
			$this->addExcludedNode($value);
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function getExcludedNodes()
	{
		return $this->excludedNodes;
	}

	/**
	 * @param Node $excludedNode
	 *
	 * @return self
	 */
	public function addExcludedNode(Node $excludedNode)
	{
		$this->excludedNodes[md5($excludedNode->getPath())] = $excludedNode;

		return $this;
	}

	/**
	 * @param bool $bool
	 *
	 * @return self
	 */
	public function setRecursiveDelete($bool)
	{
		$this->recursiveDelete = $bool;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function getRecursiveDelete()
	{
		return $this->recursiveDelete;
	}

}