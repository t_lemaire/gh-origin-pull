<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;
use GorillaHub\FilesBundle\Domain\Node;

/**
 * This describes an action to be taken by the origin pull service.  An action is to either move or delete the
 * specified files.  The files that are moved or deleted are specified by
 *
 *      {$oldParentDir}/{$glob}
 *
 * for example, a file
 *
 *      {$oldParentDir}/{$relativePath}
 *
 * can either be deleted or moved to
 *
 *      {$newParentDir}/{$relativePath}.
 *
 * For example, if we have:
 *
 *      $oldParentDir           '/import/c1234/htdocs/videos/1234'
 *      $glob                   '/flipbooks/*.jpg'
 *      $newParentDir           '/import/c1234/recyclebin/videos/1234'
 *
 * then the file:
 *
 *      /import/c1234/htdocs/videos/1234/flipbooks/5.jpg
 *
 * would be moved to:
 *
 *      /import/c1234/recyclebin/videos/1234/flipbooks/5.jpg
 *
 * and the necessary directories will be created.
 */
class DisposalAction
{

    const ACTIONTYPE_MOVE = 'move';

    const ACTIONTYPE_DELETE = 'delete';

    /**
     * @var string A glob of files on which to operate, relative to the parent directory.
     *      Examples: '*', '/origin_*', 'originals/*'.  The leading slash, if any, is ignored.
     */
    private $glob;

    /**
     * @var Node|null If this is not null, it overrides the $oldParentDir of the disposal job.
     */
    private $oldParentDir = null;

    /**
     * @var Node|null If this is not null, then the files in the old parent directory that match the specified
     *      glob are moved to this directory.
     */
    private $newParentDir;

    /**
     * @var string One of the ACTIONTYPE_* constants.  This is purely an informational field intended to guard against
     *      accidental misuse of the API.  If this is "move", then $newParentDir must not be null and must point
     *      to the target directory.  If this is "delete", then $newParentDir must be null.  Otherwise the call
     *      will fail.
     */
    private $actionType;

    /**
     * @return string A glob of files on which to operate, relative to the parent directory.
     *      Examples: '*', '/origin_*', 'originals/*'.  The leading slash, if any, is ignored.
     */
    public function getGlob()
    {
        return $this->glob;
    }

    /**
     * @param string $glob A glob of files on which to operate, relative to the parent directory.
     *      Examples: '*', '/origin_*', 'originals/*'.  The leading slash, if any, is ignored.
     * @return $this
     */
    public function setGlob($glob)
    {
        $this->glob = $glob;
        return $this;
    }

    /**
     * @return Node|null If this is not null, it overrides the $oldParentDir of the disposal job.
     */
    public function getOldParentDir()
    {
        return $this->oldParentDir;
    }

    /**
     * @param Node|null $oldParentDir If this is not null, it overrides the $oldParentDir of the disposal job.
     * @return $this
     */
    public function setOldParentDir($oldParentDir)
    {
        $this->oldParentDir = $oldParentDir;
        return $this;
    }



    /**
     * @return Node|null If this is not null, then the files in the old parent directory that match the specified
     *      glob are moved to this directory.
     */
    public function getNewParentDir()
    {
        return $this->newParentDir;
    }

    /**
     * @param Node|null $newParentDir If this is not null, then the files in the old parent directory that match the
     *      specified glob are moved to this directory.
     * @return $this
     */
    public function setNewParentDir($newParentDir)
    {
        $this->newParentDir = $newParentDir;
        return $this;
    }

    /**
     * @return string Either 'move' or 'delete'.  This is purely an informational field intended to guard against
     *      accidental misuse of the API.  If this is "move", then $newParentDir must not be null and must point
     *      to the target directory.  If this is "delete", then $newParentDir must be null.  Otherwise the call
     *      will fail.
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * @param string $actionType Either 'move' or 'delete'.  This is purely an informational field intended to guard
     *      against accidental misuse of the API.  If this is "move", then $newParentDir must not be null and must
     *      point to the target directory.  If this is "delete", then $newParentDir must be null.  Otherwise the call
     *      will fail.
     * @return $this
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }


}