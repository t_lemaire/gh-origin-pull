<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions;


use GorillaHub\FilesBundle\Domain\Node;

abstract class NodeAction
{

    /** @var  Node  $originNode*/
    private $originNode;

    /** @var  Node  $destinationNode*/
    private $destinationNode;

    /** @var bool @see setMergeDirectories() */
    private $mergeDirectories = false;

    /** @var bool @see setOverwrite() */
    private $overwrite = true;

    /**
     * @return Node
     */
    public final function getOriginNode()
    {
        return $this->originNode;
    }

    /**
     * @param Node $originNode
     */
    public final  function setOriginNode($originNode)
    {
        $this->originNode = $originNode;
    }

    /**
     * @return Node
     */
    public final  function getDestinationNode()
    {
        return $this->destinationNode;
    }

    /**
     * @param Node $destinationNode
     */
    public final  function setDestinationNode($destinationNode)
    {
        $this->destinationNode = $destinationNode;
    }

    /**
     * @return boolean @see setMergeDirectories()
     */
    public function getMergeDirectories()
    {
        return $this->mergeDirectories;
    }

    /**
     * @param boolean $mergeDirectories If this is true, and if the source and destination nodes exist on disk and
     *      are directories, then the individual files in the source directory (or any subdirectory) are copied or
     *      moved to the destination directory (or the corresponding subdirectory) iff the files don't already exist
     *      in their destination location.  Otherwise, the behaviour is the default behaviour of "cp" or "mv", which
     *      is to copy or move the source directory so that it becomes a subdirectory of the destination directory.
     * @return $this
     */
    public function setMergeDirectories($mergeDirectories)
    {
        $this->mergeDirectories = $mergeDirectories;
        return $this;
    }

    /**
     * @return boolean @see setOverwrite()
     */
    public function getOverwrite()
    {
        return $this->overwrite;
    }

    /**
     * @param bool $overwrite If this is true, and if the target file or directory already exists, or (if
     *      setMergeDirectories(true) was called) if some file in a subdirectory already exists, overwrite it.
     *      If this is false, then no files will be overwritten.
     * @return $this
     */
    public function setOverwrite($overwrite)
    {
        $this->overwrite = $overwrite;
    }

}