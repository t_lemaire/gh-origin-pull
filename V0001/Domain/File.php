<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

class File
{

	const STATUS_DOWNLOADED = 'downloaded';

	const STATUS_FAILED = 'failed';

	const STATUS_QUEUED = 'queued';

	const STATUS_DELETED = 'deleted'; // success

	const STATUS_NOT_FOUND = 'not_found'; // success

	/** @var string */
	private $jobId = '';

	/** @var string */
	private $url = '';

	/** @var string */
	private $path = '';

	/** @var string */
	private $fileName = '';

	/** @var int */
	private $permissions = 0766;

	/** @var string */
	private $status = self::STATUS_QUEUED;
    /**
     * @var string
     */
    private $volumeName = 'default';

    /**
     * @return string
     */
    public function getVolumeName()
    {
        return $this->volumeName;
    }

    /**
     * @param string $volumeName
     * @return File
     */
    public function setVolumeName($volumeName)
    {
        $this->volumeName = $volumeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        if (!isset($this->id)) {
			$identifyingProperties = get_object_vars($this);
			unset($identifyingProperties['status']);
			$this->id = md5(implode('',$identifyingProperties));
        }
        return $this->id;
	}

	/**
	 * @param string $jobId
	 * @return self
	 */
	public function setJobId($jobId)
	{
		$this->jobId = $jobId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getJobId()
	{
		return $this->jobId;
	}

	/**
	 * @param string $fileName
	 * @return self
	 */
	public function setFileName($fileName)
	{
		$this->fileName = $fileName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->fileName;
	}

	/**
	 * @param string $path
	 * @return self
	 */
	public function setPath($path)
	{
		$this->path = $path;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @param int $permissions
	 *
	 * @return self
	 */
	public function setPermissions($permissions)
	{
		$this->permissions = $permissions;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getPermissions()
	{
		return $this->permissions;
	}

	/**
	 * @param string $url
	 *
	 * @return self
	 */
	public function setUrl($url)
	{
		$scheme = parse_url($url, PHP_URL_SCHEME);
		if (!is_string($scheme)) {
			$url = 'http://' . ltrim($url, '/');
		}
		$this->url = $url;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @return bool
	 */
	public function isProcessed()
	{
		return $this->status !== self::STATUS_QUEUED;
	}

	/**
	 * @return bool
	 */
	public function wasSuccessful()
	{
		return in_array(
			$this->status,
			array(self::STATUS_DELETED, self::STATUS_NOT_FOUND, self::STATUS_DOWNLOADED)
		);
	}

	/**
	 * @return bool
	 */
	public function wasUnsuccessful()
	{
		return $this->status === self::STATUS_FAILED;
	}
}