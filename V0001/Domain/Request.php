<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

abstract class Request {
    /**
     * @var Signature
     */
    private  $signature;

    /**
     * @var string
     */
    private $callbackUrl = '';


    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $callbackUrl
     *
     * @return self
     */
    public function setCallbackUrl($callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->callbackUrl;
    }
}