<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

class NodeFailure {

	/* @var \GorillaHub\FilesBundle\Domain\Node */
	private $node;

	/* @var String */
	private $message;

	/**
	 * @param String $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return String
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param \GorillaHub\FilesBundle\Domain\Node $node
	 */
	public function setNode($node)
	{
		$this->node = $node;
	}

	/**
	 * @return \GorillaHub\FilesBundle\Domain\Node
	 */
	public function getNode()
	{
		return $this->node;
	}



} 