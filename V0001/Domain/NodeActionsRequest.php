<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;


use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\NodeAction;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;

class NodeActionsRequest  extends Request implements SDKCallInterface
{

    /** @var  NodeAction[] */
    private $actions;

    /**
     * @var string
     */
    private $referenceId = '';

    /**
     * @return NodeActions\NodeAction[]
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param NodeActions\NodeAction[] $actions
     * @return $this
     */
    public function setActions($actions)
    {
        foreach ($actions as $key => $action) {
            $this->addAction($action);
        }
        return $this;
    }


    /**
     * @param NodeActions\NodeAction $action
     */
    public function addAction($action){
        if(null == $this->actions){
            $this->actions = array();
        }
        $this->actions[md5($action->getDestinationNode()->getPath())] = $action;
    }

    /**
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param string $referenceId
     * @return $this
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
        return $this;
    }



}