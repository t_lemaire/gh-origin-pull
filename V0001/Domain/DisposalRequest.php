<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\FilesBundle\Domain\Node;

/**
 * This request tells the origin pull to delete or move some files.  The deletions are done with a special user
 * that has access to various "toxic waste dump" directories, which are configured so that files can be put into
 * the directory but never retrieved from it.
 */
class DisposalRequest extends Request implements SDKCallInterface
{

    /** @var DisposalAction[] The actions, in the order in which they are executed. */
    private $actions = [];

    /**
     * @var Node The parent directory on which to operate.
     */
    private $oldParentDir;

    /**
     * @var int The minimum depth of the path in $oldParentDir.  If $oldParentDir is shorter than this, the request
     *      fails.
     */
    private $minimumPathDepth = 5;

    /**
     * @var int If there are more than this number of files in $oldParentDir, the request fails.
     */
    private $maxNumberOfFiles = 1000;

    /**
     * @return DisposalAction[] The actions, in the order in which they are executed.
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param DisposalAction[] $actions The actions, in the order in which they are executed.
     * @return $this
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
        return $this;
    }


    /**
     * @param DisposalAction $action An action that will be executed after all of the actions that were already
     *      added.
     */
    public function addAction($action){
        $this->actions[] = $action;
    }

    /**
     * @return Node The parent directory on which to operate.
     */
    public function getOldParentDir()
    {
        return $this->oldParentDir;
    }

    /**
     * @param Node $oldParentDir The parent directory on which to operate.
     * @return $this
     */
    public function setOldParentDir(Node $oldParentDir)
    {
        $this->oldParentDir = $oldParentDir;
        return $this;
    }

    /**
     * @return int The minimum depth of the path in $oldParentDir.  If $oldParentDir is shorter than this, the request
     *      fails.
     */
    public function getMinimumPathDepth()
    {
        return $this->minimumPathDepth;
    }

    /**
     * @param int $minimumPathDepth The minimum depth of the path in $oldParentDir.  If $oldParentDir is shorter than
     *      this, the request fails.
     * @return $this
     */
    public function setMinimumPathDepth($minimumPathDepth)
    {
        $this->minimumPathDepth = $minimumPathDepth;
        return $this;
    }

    /**
     * @return int If there are more than this number of files in $oldParentDir, the request fails.
     */
    public function getMaxNumberOfFiles()
    {
        return $this->maxNumberOfFiles;
    }

    /**
     * @param int $maxNumberOfFiles If there are more than this number of files in $oldParentDir, the request fails.
     * @return $this
     */
    public function setMaxNumberOfFiles($maxNumberOfFiles)
    {
        $this->maxNumberOfFiles = $maxNumberOfFiles;
        return $this;
    }



}