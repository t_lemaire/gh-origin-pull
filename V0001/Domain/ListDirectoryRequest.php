<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Node\Directory;

class ListDirectoryRequest extends Request implements SDKCallInterface
{

	/**
	 * @var Directory
	 */
	private $directory = null;

	/**
	 * @param Directory $directory
	 *
	 * @return $this
	 */
	public function setDirectory(Directory $directory)
	{
		$this->directory = $directory;

		return $this;
	}

	/**
	 * @return Directory
	 */
	public function getDirectory()
	{
		return $this->directory;
	}

}