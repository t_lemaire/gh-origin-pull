<?php
namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use \GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions\PullRequestNotFullyDefinedException;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use \GorillaHub\SDKs\OriginPullBundle\V0001\Exceptions\JobIdMissingInFileException;

/**
 * Class PullRequest
 * @Annotation
 * @package GorillaHub\SDKs\OriginPullBundle\V0001\Domain
 */
class PullRequest implements SDKCallInterface
{

	/**
	 * @var File[]
	 */
	private $files = array();

	/**
	 * @var Signature
	 */
	private $signature;

	/**
	 * @var string
	 */
	private $callbackUrl = '';

	/**
	 * @var int
	 */
	private $referenceId = 0;

	/**
	 * @var FilePattern
	 */
	private $filePattern;

	/**
	 * @param \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern $pattern
	 *
	 * @return $this
	 */
	public function setFilePattern($pattern)
	{
		$this->filePattern = $pattern;

		return $this;
	}

	/**
	 * @return \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern
	 */
	public function getFilePattern()
	{
		return $this->filePattern;
	}

	/**
	 * @param File[] $files
	 *
	 * @return self
	 */
	public function setFiles($files)
	{
		foreach ($files as $key => $value) {
			$this->addFile($value);
		}
		
		return $this;
	}

	/**
	 * @return File[]
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * @param File $file
	 *
	 * @return $this
	 * @throws JobIdMissingInFileException
	 * @throws PullRequestNotFullyDefinedException
	 */
	public function addFile(File $file)
	{
		$fileJobId = $file->getJobId();

		if (strlen($fileJobId) == 0) {
			throw new JobIdMissingInFileException('File is missing Job Id definition.');
		}

		if (strlen($file->getJobId()) == 0) {
			$file->setJobId($this->getReferenceId());
		}

		$this->files[$file->getId()] = $file;

		return $this;
	}

	/**
	 * @param File $file
	 *
	 * @return self
	 */
	public function removeFile($file)
	{
		unset($this->files[$file->getId()]);
		return $this;
	}

	/**
	 * Sets the signature.
	 *
	 * @param Signature $signature
	 *
	 * @return self
	 */
	public function setSignature(Signature $signature)
	{
		$this->signature = $signature;

		return $this;
	}

	/**
	 * Returns the signature.
	 *
	 * @return Signature
	 */
	public function getSignature()
	{
		return $this->signature;
	}

	/**
	 * @param string $callbackUrl
	 *
	 * @return self
	 */
	public function setCallbackUrl($callbackUrl)
	{
		$this->callbackUrl = $callbackUrl;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCallbackUrl()
	{
		return $this->callbackUrl;
	}

	/**
	 * @param int $referenceId
	 *
	 * @return $this
	 */
	public function setReferenceId($referenceId)
	{
		$this->referenceId = $referenceId;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getReferenceId()
	{
		return $this->referenceId;
	}

	/**
	 * @return bool
	 */
	public function areAllFilesProcessed()
	{
		foreach ($this->files as $file) {
			if ($file->isProcessed() === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function wereAllFilesSuccessful()
	{
		foreach ($this->files as $file) {
			if ($file->wasSuccessful() === false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return bool
	 */
	public function wereAllFilesUnsuccessful()
	{
		foreach ($this->files as $file) {
			if ($file->wasUnsuccessful() === false) {
				return false;
			}
		}
		return true;
	}
}