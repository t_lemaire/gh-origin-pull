<?php

namespace GorillaHub\SDKs\OriginPullBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;

/**
 * A request of this type can be sent to /originpull/restoreillegal/.  The origin pull will attempt to find the file
 * in one of the listed file locations and put it in a temporary storage location that is served by a web server.
 * The response of that request will be a RestoreIllegalResult or a FailResult.  The call's timeout should be high
 * enough to allow the file to be copied (say, 60s).
 */
class RestoreIllegalRequest extends Request implements SDKCallInterface
{

    /**
     * @var string[] The paths (e.g. "/import/c1234/videos/202001/01/1234567/4.mp4") at which the file might be
     *      located.  The first path that actually points to a file is used as the source path.
     */
    private $possiblePaths = [];

    /**
     * @return string[] The paths (e.g. "/import/c1234/videos/202001/01/1234567/4.mp4") at which the file might be
     *      located.  The first path that actually points to a file is used as the source path.
     */
    public function getPossiblePaths()
    {
        return $this->possiblePaths;
    }

    /**
     * @param string[] $possiblePaths The paths (e.g. "/import/c1234/videos/202001/01/1234567/4.mp4") at which the
     *      file might be located.  The first path that actually points to a file is used as the source path.
     * @return $this
     */
    public function setPossiblePaths($possiblePaths)
    {
        $this->possiblePaths = $possiblePaths;
        return $this;
    }


}