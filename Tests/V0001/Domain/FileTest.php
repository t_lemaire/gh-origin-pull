<?php
namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain;

use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\File;

class FileTest extends \PHPUnit_Framework_TestCase
{
    public function testSettingJobId()
    {
        $file = new File();

        $jobId = 'aca6f8a85fc40b1ae58dab2f68ebad0c';

        $file->setJobId($jobId);
        $this->assertEquals($jobId, $file->getJobId());
    }

	public function testSettingFilename()
	{
		$file = new File();

		$filename = 'filename.jpg';

		$file->setFileName($filename);
		$this->assertEquals($filename, $file->getFileName());
	}

	public function testSettingPath()
	{
		$file = new File();

		$path = '\images\filename.jpg';

		$file->setPath($path);
		$this->assertEquals($path, $file->getPath());
	}

	public function testSettingPermissions()
	{
		$file = new File();

		$permissions = '444';

		$file->setPermissions($permissions);
		$this->assertEquals($permissions, $file->getPermissions());
	}

	public function testSettingUrl()
	{
		$file = new File();

		$url = 'http://url';

		$file->setUrl($url);
		$this->assertEquals($url, $file->getUrl());
	}

    public function testSettingStatus()
    {
        $file = new File();

        $status = '200';

        $file->setStatus($status);
        $this->assertEquals($status, $file->getStatus());
    }

	public function testFileIdDoesNotDependOnStatus()
	{
		$file = new File();
		$file->setUrl('http://whatever.whatever/asdf');
		$file->setPath('/tmp/nothing');
		$file->setFileName("test.me");
		$file->setJobId('1');
		$file->setStatus(File::STATUS_QUEUED);

		$fileWithDifferentURL = clone $file;
		$fileWithDifferentURL->setUrl('http://another.host');

		$fileWithDifferentFilename = clone $file;
		$fileWithDifferentFilename->setFileName('another.filename');

		$fileWithDifferentPath = clone $file;
		$fileWithDifferentPath->setPath('/tmp/different');

		$fileWithDifferentJob = clone $file;
		$fileWithDifferentJob->setJobId('2');

		$fileWithDifferentStatus = clone $file;
		$fileWithDifferentStatus->setStatus(File::STATUS_DELETED);

		$this->assertNotEquals($file->getId(), $fileWithDifferentFilename->getId());
		$this->assertNotEquals($file->getId(), $fileWithDifferentURL->getId());
		$this->assertNotEquals($file->getId(), $fileWithDifferentJob->getId());
		$this->assertNotEquals($file->getId(), $fileWithDifferentPath->getId());
		$this->assertEquals($file->getId(), $fileWithDifferentStatus->getId());
	}

}

