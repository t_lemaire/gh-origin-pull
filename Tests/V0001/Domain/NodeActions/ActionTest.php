<?php
namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain\NodeActions;

use GorillaHub\FilesBundle\Domain\File;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\NodeAction;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\Copy;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\Move;

class ActionTest  extends \PHPUnit_Framework_TestCase
{

    public function testIntegrity(){
        $move = new Move();
        $this->assertTrue($move instanceof NodeAction);

        $copy = new Copy();
        $this->assertTrue($copy instanceof NodeAction);
    }


    public function testAction(){
        $action = new Move();

        $originNode = new File('/path/to/file1');
        $originNode->setClientId(6);
        $destinatioNode = new File('/path/to/file2');
        $destinatioNode->setClientId(6);

        $action->setOriginNode($originNode);
        $action->setDestinationNode($destinatioNode);

        $this->assertEquals($originNode, $action->getOriginNode());
        $this->assertEquals($destinatioNode, $action->getDestinationNode());
    }
}