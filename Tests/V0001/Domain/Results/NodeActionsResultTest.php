<?php

namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain\Results;


use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\NodeActions\NodeActionsResult;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\NodeActions\NodeResult;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

class NodeActionsResultTest extends \PHPUnit_Framework_TestCase
{

    public function testNodeResult(){
        $nodeResult1 = new NodeResult();
        $nodeResult1->setStatus(NodeResult::STATUS_COPIED);
        $nodeResult1->setPath('path/to/the/file');

        $this->assertEquals(true, $nodeResult1->wasSuccessful());
        $this->assertEquals(false, $nodeResult1->wasUnsuccessful());


        $nodeResult2 = new NodeResult();
        $nodeResult2->setStatus(NodeResult::STATUS_MOVED);
        $nodeResult2->setPath('path/to/the/file2');

        $this->assertEquals(true, $nodeResult2->wasSuccessful());
        $this->assertEquals(false, $nodeResult2->wasUnsuccessful());


        $nodeResult3 = new NodeResult();
        $nodeResult3->setStatus(NodeResult::STATUS_FAILED);
        $nodeResult3->setPath('path/to/the/file3');

        $this->assertEquals(false, $nodeResult3->wasSuccessful());
        $this->assertEquals(true, $nodeResult3->wasUnsuccessful());
    }

    public function testNewResult(){

        $signature = new Signature();
        $signature->setClientId(6);
        $signature->setJobId(md5('requestForCllient6'));

        $nodeResult1 = new NodeResult();
        $nodeResult1->setStatus(NodeResult::STATUS_COPIED);
        $nodeResult1->setPath('path/to/the/file');

        $nodeResult2 = new NodeResult();
        $nodeResult2->setStatus(NodeResult::STATUS_MOVED);
        $nodeResult2->setPath('path/to/the/file2');

        $nodeResult3 = new NodeResult();
        $nodeResult3->setStatus(NodeResult::STATUS_FAILED);
        $nodeResult3->setPath('path/to/the/file3');

        $result = new NodeActionsResult();
        $result->setSignature($signature);


        $result->addNodeResult($nodeResult1);
        $result->addNodeResult($nodeResult2);
        $result->addNodeResult($nodeResult3);

        $this->assertEquals(3, count($result->getNodeResults()));
    }



}