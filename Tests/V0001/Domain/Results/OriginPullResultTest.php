<?php

namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain\Results;


use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\File;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\Results\OriginPullResult;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;

class OriginPullResultTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingFiles()
    {
        $originPullResult = new OriginPullResult();

        $file1 = new File();
        $file1->setUrl('http://123');
        $file1->setFileName('file1.png');

        $file2 = new File();
        $file2->setUrl('http://456');
        $file2->setFileName('file2.mp4');

        $expectedFiles = array();
        $expectedFiles[md5($file1->getUrl())] = $file1;
        $expectedFiles[md5($file2->getUrl())] = $file2;

        $originPullResult->setFiles(array($file1, $file2));
        $this->assertEquals($expectedFiles, $originPullResult->getFiles());
    }

    public function testSettingAReferencedId()
    {
        $originPullResult = new OriginPullResult();
        $referenceId = 6;
        $originPullResult->setReferenceId($referenceId);
        $this->assertEquals($referenceId, $originPullResult->getReferenceId());
    }

    public function testSettingAFilePattern()
    {
        $originPullResult = new OriginPullResult();
        $filePattern = new FilePattern();
        $filePattern->setPathPattern('http://123/file1.png');
        $originPullResult->setFilePattern($filePattern);
        $this->assertEquals($filePattern, $originPullResult->getFilePattern());
    }

    public function testSettingBasePath()
    {
        $originPullResult = new OriginPullResult();
        $basePath = 'http://123';
        $originPullResult->setBasePath($basePath);
        $this->assertEquals($basePath, $originPullResult->getBasePath());
    }
} 