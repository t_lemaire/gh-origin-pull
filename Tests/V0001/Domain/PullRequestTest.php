<?php
namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain;

use \GorillaHub\JSONSerializerBundle\JSONSerializer;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\AudioMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Preview;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Watermark;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\File;
use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\PullRequest;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use \GorillaHub\SDKs\UploadBundle\V0001\Domain\Jobs\Video\VideoUploadJob;

class PullRequestTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFiles()
	{
		$pullRequest = new PullRequest();

		$file1 = new File();
		$file1->setUrl('http://123');
		$file1->setFileName('file1.png');
		$file1->setJobId(md5(1));

		$file2 = new File();
		$file2->setUrl('http://456');
		$file2->setFileName('file2.mp4');
		$file2->setJobId(md5(1));

		$expectedFiles = array();
		$expectedFiles[$file1->getId()] = $file1;
		$expectedFiles[$file2->getId()] = $file2;

		$pullRequest->setFiles(array($file1, $file2));
		$this->assertEquals($expectedFiles, $pullRequest->getFiles());
	}

	public function testAddingAFile()
	{
		$pullRequest = new PullRequest();

		$file1 = new File();
		$file1->setUrl('http://123');
		$file1->setFileName('file1.png');
		$file1->setJobId(md5(1));

		$file2 = new File();
		$file2->setUrl('http://456');
		$file2->setFileName('file2.mp4');
		$file2->setJobId(md5(1));

		$expectedFiles   = array();
		$expectedFiles[$file1->getId()] = $file1;
		$expectedFiles[$file2->getId()] = $file2;

		$pullRequest->addFile($file1);
		$pullRequest->addFile($file2);

		$this->assertEquals($expectedFiles, $pullRequest->getFiles());
	}

    public function testSettingAFilePattern()
    {
        $pullRequest = new PullRequest();
        $filePattern = new FilePattern ();
        $filePattern->setPathPattern('http://123/file1.png');
        $pullRequest->setFilePattern($filePattern);
        $this->assertEquals($filePattern, $pullRequest->getFilePattern());
    }

    public function testSettingASignature()
    {
        $pullRequest = new PullRequest();
        $signature = new Signature();
        $signature->setJobId('aca6f8a85fc40b1ae58dab2f68ebad0c');
        $signature->setTransactionId('0');
        $signature->setClientId(6);
        $pullRequest->setSignature($signature);
        $this->assertEquals($signature, $pullRequest->getSignature());
    }

    public function testSettingACallbackUrl()
    {
        $pullRequest = new PullRequest();
        $callbackUrl = 'http://123/callback';
        $pullRequest->setCallbackUrl($callbackUrl);
        $this->assertEquals($callbackUrl, $pullRequest->getCallbackUrl());
    }

    public function testSettingAReferencedId()
    {
        $pullRequest = new PullRequest();
        $referenceId = 6;
        $pullRequest->setReferenceId($referenceId);
        $this->assertEquals($referenceId, $pullRequest->getReferenceId());
    }
}