<?php

use \GorillaHub\SDKs\OriginPullBundle\V0001\Domain\DeleteRequest;
use \GorillaHub\FilesBundle\Domain\File;
use \GorillaHub\FilesBundle\Domain\Path;

class DeleteRequestTest extends \PHPUnit_Framework_TestCase
{
    public function testSettingFiles()
    {
        $deleteRequest = new DeleteRequest();

        $file1 = new File();

		$path = new Path();
		$path->setPath('/123/file1.png');
        $file1->setPath($path);

        $file2 = new File();
		$path = new Path();
		$path->setPath('/456/file2.mp4');
        $file2->setPath($path);

        $expectedFiles = array();
        $expectedFiles[md5($file1->getPath()->getPath())] = $file1;
        $expectedFiles[md5($file2->getPath()->getPath())] = $file2;

        $deleteRequest->setNodes(array($file1, $file2));
        $this->assertEquals($expectedFiles, $deleteRequest->getNodes());
    }

    public function testAddingAFile()
    {
        $deleteRequest = new DeleteRequest();

        $file1 = new File();
		$path = new Path();
		$path->setPath('/123/file1.png');
		$file1->setPath($path);

        $file2 = new File();
		$path = new Path();
		$path->setPath('/456/file2.mp4');
		$file2->setPath($path);

        $expectedFiles   = array();
        $expectedFiles[md5($file1->getPath()->getPath())] = $file1;
        $expectedFiles[md5($file2->getPath()->getPath())] = $file2;

        $deleteRequest->addNode($file1);
        $deleteRequest->addNode($file2);

        $this->assertEquals($expectedFiles, $deleteRequest->getNodes());
    }

    public function testSettingCallBack(){
        $deleteRequest = new DeleteRequest();

        $callBackUrl = 'http://123.com/callback/';
        $deleteRequest->setCallbackUrl($callBackUrl);

        $this->assertEquals($callBackUrl, $deleteRequest->getCallBackUrl());
    }


}