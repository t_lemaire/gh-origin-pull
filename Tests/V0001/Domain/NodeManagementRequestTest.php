<?php


namespace GorillaHub\SDKs\OriginPullBundle\Tests\V0001\Domain;


use GorillaHub\FilesBundle\Domain\File;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\Copy;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActions\Move;
use GorillaHub\SDKs\OriginPullBundle\V0001\Domain\NodeActionsRequest;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

class NodeManagementRequestTest extends \PHPUnit_Framework_TestCase
{

    public function testNodeManagementRequest()
    {

        $request = $this->createDefaultRequest();
        $originNode = new File('/path/to/file1');
        $originNode->setClientId(6);


        $destinatioNode = new File('/path/to/file2');
        $destinatioNode->setClientId(6);

        $action1 = new Copy();
        $action1->setOriginNode($originNode);
        $action1->setDestinationNode($destinatioNode);

        $request->addAction($action1);

        $actionFromRequest = $request->getActions();

        $this->assertEquals(1, count($actionFromRequest));

        $action = array_shift($actionFromRequest);

        $this->assertEquals($action->getOriginNode(), $action1->getOriginNode());
        $this->assertEquals($action->getDestinationNode(), $action1->getDestinationNode());

    }

    public function testAdding2NodesWithSameDestination()
    {
        $request = $this->createDefaultRequest();
        $originNode = new File('/path/to/file1');
        $originNode->setClientId(6);

        $destinatioNode = new File('/path/to/file2');
        $destinatioNode->setClientId(6);

        $action1 = new Copy();
        $action1->setOriginNode($originNode);
        $action1->setDestinationNode($destinatioNode);

        $request->addAction($action1);

        $this->assertEquals(1, count($request->getActions()));

        $request->addAction($action1);

        $this->assertEquals(1, count($request->getActions()), 'the request should avoid duplicate action');

    }

    public function testAdding2Actions()
    {
        $request = $this->createDefaultRequest();
        $originNode = new File('/path/to/file1');
        $originNode->setClientId(6);

        $destinatioNode = new File('/path/to/file2');
        $destinatioNode->setClientId(6);

        $action1 = new Copy();
        $action1->setOriginNode($originNode);
        $action1->setDestinationNode($destinatioNode);

        $request->addAction($action1);

        $this->assertEquals(1, count($request->getActions()));

        $action2 = new Move();
        $action2->setOriginNode($destinatioNode);
        $action2->setDestinationNode($originNode);

        $request->addAction($action2);

        $this->assertEquals(2, count($request->getActions()));

    }


    private function createDefaultRequest()
    {
        $signature = new Signature();
        $signature->setClientId(6);
        $signature->setJobId(md5('requestForCllient6'));

        $request = new NodeActionsRequest();
        $request->setSignature($signature);
        $request->setCallbackUrl('http://localhost');

        return $request;
    }
}